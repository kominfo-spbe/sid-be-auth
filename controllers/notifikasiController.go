package controllers

import (
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm/clause"
	"math"
	"net/url"
	"sidat-auth/database"
	"sidat-auth/models"
	"sidat-auth/models/reqresp"
	"sidat-auth/util"
	"strconv"
)

// AllNotifikasi Notifikasi list
// @Summary Notifikasi list
// @Description Notifikasi list
// @Tags Notifikasi
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param user_id query int false "User ID"
// @param sort query string false "Sort by"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.DxDatagridResponse
// @Router /notifikasi [get]
func AllNotifikasi(c *fiber.Ctx) error {
	limit, _ := strconv.Atoi(c.Query("take", "10"))
	offset, _ := strconv.Atoi(c.Query("skip", "0"))
	page := math.Ceil(float64(offset/limit)) + 1
	var total int64

	userId := c.Query("user_id", "")
	sortStr := c.Query("sort", "")

	var Notifikasi []models.Notifikasi

	baseQ := database.DB.Preload(clause.Associations)
	if userId != "" {
		baseQ = baseQ.Where("user_id = ?", userId)
	}

	if sortStr != "" {
		sortParsed, err := url.PathUnescape(sortStr)
		if err != nil {
			return err
		}
		var s []reqresp.SortRequest
		err = json.Unmarshal([]byte(sortParsed), &s)
		if err != nil {
			return err
		}
		if s[0].Desc {
			baseQ = baseQ.Order(s[0].Selector + " desc")
		} else {
			baseQ = baseQ.Order(s[0].Selector)
		}
	}

	baseQ.Offset(offset).Limit(limit).Find(&Notifikasi)

	baseQ.Model(&models.Notifikasi{}).Count(&total)

	lastPage := math.Ceil(float64(int(total) / limit))
	if lastPage <= 0 {
		lastPage = 1
	}

	return c.JSON(&reqresp.DxDatagridResponse{
		Data:       Notifikasi,
		TotalCount: int(total),
		Summary:    nil,
		Meta: fiber.Map{
			"total":     total,
			"page":      page,
			"last_page": lastPage,
		},
	})
}

// CreateNotifikasi Create a notifikasi
// @Summary Create a notifikasi
// @Description Create a notifikasi
// @Tags Notifikasi
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param notifikasi body models.Notifikasi true "Data notifikasi"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /notifikasi [post]
func CreateNotifikasi(c *fiber.Ctx) error {
	var record models.Notifikasi

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	result := database.DB.Create(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to create record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "notifikasi", "insert", nil, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success creating record",
		Data:    record,
	})
}

// GetNotifikasi Get single Notifikasi data
// @Summary Get single Notifikasi data
// @Description Get single Notifikasi data
// @Tags Notifikasi
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Notifikasi ID"
// @Success 200 {object} models.Notifikasi
// @Error 404 {object} reqresp.ErrorResponse
// @Router /notifikasi/{id} [get]
func GetNotifikasi(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	Notifikasi := models.Notifikasi{Id: uint(id)}

	result := database.DB.Preload(clause.Associations).Find(&Notifikasi)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(Notifikasi)
}

// UpdateNotifikasi Update a Notifikasi
// @Summary Update a Notifikasi
// @Description Update a Notifikasi
// @Tags Notifikasi
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Notifikasi ID"
// @param Notifikasi body models.Notifikasi true "Data notifikasi"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /notifikasi/{id} [put]
func UpdateNotifikasi(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.Notifikasi{Id: uint(id)}

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	var oldData models.Notifikasi
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Model(&record).Updates(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to update record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "notifikasi", "update", &oldData, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success updating record",
		Data:    record,
	})
}

// DeleteNotifikasi Delete a Notifikasi
// @Summary Delete a Notifikasi
// @Description Delete a Notifikasi
// @Tags Notifikasi
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Notifikasi ID"
// @Success 204 {object} reqresp.SuccessResponse
// @Error 500 {object} reqresp.ErrorResponse
// @Router /notifikasi/{id} [delete]
func DeleteNotifikasi(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	notifikasi := models.Notifikasi{Id: uint(id)}

	var oldData models.Notifikasi
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Delete(&notifikasi)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to delete record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "notifikasi", "delete", &oldData, nil)

	c.Status(fiber.StatusNoContent)
	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "notifikasi record deleted",
		Data:    notifikasi,
	})
}
