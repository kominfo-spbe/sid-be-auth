package controllers

import (
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm/clause"
	"math"
	"net/url"
	"sidat-auth/database"
	"sidat-auth/models"
	"sidat-auth/models/reqresp"
	"sidat-auth/util"
	"strconv"
	"strings"
)

// AllPermissions Permission list
// @Summary Permission list
// @Description Permission list
// @Tags Permissions
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param nama query string false "Name to search"
// @param sort query string false "Sort by"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.DxDatagridResponse
// @Router /permissions [get]
func AllPermissions(c *fiber.Ctx) error {
	limit, _ := strconv.Atoi(c.Query("take", "10"))
	offset, _ := strconv.Atoi(c.Query("skip", "0"))
	page := math.Ceil(float64(offset/limit)) + 1
	var total int64

	var permission []models.Permission

	nama := c.Query("nama", "")
	sortStr := c.Query("sort", "")

	baseQ := database.DB.Preload(clause.Associations)
	if nama != "" {
		baseQ = baseQ.Where("UPPER(name) LIKE ?", "%"+ strings.ToUpper(nama) +"%")
	}

	if sortStr != "" {
		sortParsed, err := url.PathUnescape(sortStr)
		if err != nil {
			return err
		}
		var s []reqresp.SortRequest
		err = json.Unmarshal([]byte(sortParsed), &s)
		if err != nil {
			return err
		}
		if s[0].Desc {
			baseQ = baseQ.Order(s[0].Selector + " desc")
		} else {
			baseQ = baseQ.Order(s[0].Selector)
		}
	}

	baseQ.Offset(offset).Limit(limit).Find(&permission)

	baseQ.Model(&models.Permission{}).Count(&total)

	lastPage := math.Ceil(float64(int(total) / limit))
	if lastPage <= 0 {
		lastPage = 1
	}

	return c.JSON(&reqresp.DxDatagridResponse{
		Data:       permission,
		TotalCount: int(total),
		Summary:    nil,
		Meta: fiber.Map{
			"total":     total,
			"page":      page,
			"last_page": lastPage,
		},
	})
}

// CreatePermission Create a permission
// @Summary Create a permission
// @Description Create a permission
// @Tags Permissions
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param permission body models.Permission true "Permission data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /permissions [post]
func CreatePermission(c *fiber.Ctx) error {
	var record models.Permission

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	result := database.DB.Create(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to create record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "permission", "insert", nil, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success creating record",
		Data:    record,
	})
}

// GetPermission Get single permission data
// @Summary Get single permission data
// @Description Get single permission data
// @Tags Permissions
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Permission ID"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 404 {object} reqresp.ErrorResponse
// @Router /permissions/{id} [get]
func GetPermission(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.Permission{Id: uint(id)}

	result := database.DB.Find(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success getting record",
		Data:    record,
	})
}

// UpdatePermission Update a permission
// @Summary Update a permission
// @Description Update a permission
// @Tags Permissions
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Permission ID"
// @param permission body models.Permission true "Permission data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /permission/{id} [put]
func UpdatePermission(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.Permission{Id: uint(id)}

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	var oldData models.Permission
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Model(&record).Updates(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to update record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "permission", "update", &oldData, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success updating record",
		Data:    record,
	})
}

// DeletePermission Delete a permission
// @Summary Delete a permission
// @Description Delete a permission
// @Tags Permissions
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Permission ID"
// @Success 204 {object} reqresp.SuccessResponse
// @Error 500 {object} reqresp.ErrorResponse
// @Router /permissions/{id} [delete]
func DeletePermission(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	permission := models.Permission{Id: uint(id)}

	var oldData models.Permission
	database.DB.Where("id = ?", id).First(&oldData)

	var res interface{}
	database.DB.Table("role_permissions").Where("permission_id", uint(id)).Delete(&res)

	result := database.DB.Delete(&permission)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to delete record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "permission", "delete", &oldData, nil)

	c.Status(fiber.StatusNoContent)
	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "permission record deleted",
		Data:    permission,
	})
}

// CheckPermission Check permission for current user
// @Summary Check permission for current user
// @Description Check permission for current user
// @Tags Permissions
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param perm_name query string true "Permission Name"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 404 {object} reqresp.ErrorResponse
// @Router /permissions/check [get]
func CheckPermission(c *fiber.Ctx) error {
	permName := c.Query("perm_name", "")

	isAllowed, _ := util.IsAllowed(c, permName)

	c.Status(fiber.StatusOK)
	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Check permission succeed",
		Data:    isAllowed,
	})
}
