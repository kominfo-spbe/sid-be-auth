package controllers

import (
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm/clause"
	"math"
	"net/url"
	"sidat-auth/database"
	"sidat-auth/models"
	"sidat-auth/models/reqresp"
	"sidat-auth/util"
	"strconv"
	"strings"
)

// AllRoles Role list
// @Summary Role list
// @Description Role list
// @Tags Roles
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param nama query string false "Name to search"
// @param sort query string false "Sort by"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.DxDatagridResponse
// @Router /roles [get]
func AllRoles(c *fiber.Ctx) error {
	limit, _ := strconv.Atoi(c.Query("take", "10"))
	offset, _ := strconv.Atoi(c.Query("skip", "0"))
	page := math.Ceil(float64(offset/limit)) + 1
	var total int64

	nama := c.Query("nama", "")
	sortStr := c.Query("sort", "")

	var roles []models.Role

	baseQ := database.DB.Preload(clause.Associations)
	if nama != "" {
		baseQ = baseQ.Where("UPPER(name) LIKE ?", "%"+ strings.ToUpper(nama) +"%")
	}

	if sortStr != "" {
		sortParsed, err := url.PathUnescape(sortStr)
		if err != nil {
			return err
		}
		var s []reqresp.SortRequest
		err = json.Unmarshal([]byte(sortParsed), &s)
		if err != nil {
			return err
		}
		if s[0].Desc {
			baseQ = baseQ.Order(s[0].Selector + " desc")
		} else {
			baseQ = baseQ.Order(s[0].Selector)
		}
	}

	baseQ.Offset(offset).Limit(limit).Find(&roles)

	baseQ.Model(&models.Role{}).Count(&total)

	lastPage := math.Ceil(float64(int(total) / limit))
	if lastPage <= 0 {
		lastPage = 1
	}

	return c.JSON(&reqresp.DxDatagridResponse{
		Data:       roles,
		TotalCount: int(total),
		Summary:    nil,
		Meta: fiber.Map{
			"total":     total,
			"page":      page,
			"last_page": lastPage,
		},
	})
}

type RoleDto struct {
	Name string `json:"name"`
	Permissions []int `json:"permissions"`
}

// CreateRole Create a role
// @Summary Create a role
// @Description Create a role
// @Tags Roles
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param role body RoleDto true "Role and permission data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /roles [post]
func CreateRole(c *fiber.Ctx) error {
	var roleDto RoleDto

	if err := c.BodyParser(&roleDto); err != nil {
		return err
	}

	list := roleDto.Permissions

	permissions := make([]models.Permission, len(list))

	for i, permissionId := range list {
		permissions[i] = models.Permission{
			Id: uint(permissionId),
		}
	}

	record := models.Role{
		Name:       roleDto.Name,
		Permission: permissions,
	}

	result := database.DB.Create(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to create record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "role", "insert", nil, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success creating record",
		Data:    record,
	})
}

// GetRole Get single role data
// @Summary Get single role data
// @Description Get single role data
// @Tags Roles
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Role ID"
// @Success 200 {object} models.Role
// @Error 404 {object} reqresp.ErrorResponse
// @Router /roles/{id} [get]
func GetRole(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	role := models.Role{Id: uint(id)}

	result := database.DB.Preload("Permission").Find(&role)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(role)
}

// UpdateRole Update a role
// @Summary Update a role
// @Description Update a role
// @Tags Roles
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Role ID"
// @param role body RoleDto true "Role data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /roles/{id} [put]
func UpdateRole(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	var roleDto RoleDto

	//record := models.Role{Id: uint(id)}

	if err := c.BodyParser(&roleDto); err != nil {
		return err
	}

	list := roleDto.Permissions

	permissions := make([]models.Permission, len(list))

	for i, permissionId := range list {
		permissions[i] = models.Permission{
			Id: uint(permissionId),
		}
	}

	var oldData models.Role
	database.DB.Where("id = ?", id).First(&oldData)

	var res interface{}
	database.DB.Table("role_permissions").Where("role_id", uint(id)).Delete(&res)

	record := models.Role{
		Id:         uint(id),
		Name:       roleDto.Name,
		Permission: permissions,
	}

	result := database.DB.Model(&record).Preload("Permission").Updates(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to update record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "role", "update", &oldData, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success updating record",
		Data:    record,
	})
}

// DeleteRole Delete a role
// @Summary Delete a role
// @Description Delete a role
// @Tags Roles
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Role ID"
// @Success 204 {object} reqresp.SuccessResponse
// @Error 500 {object} reqresp.ErrorResponse
// @Router /roles/{id} [delete]
func DeleteRole(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.Role{Id: uint(id)}

	var oldData models.Role
	database.DB.Where("id = ?", id).First(&oldData)

	var res interface{}
	database.DB.Table("role_permissions").Where("role_id", uint(id)).Delete(&res)

	result := database.DB.Delete(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to delete record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "role", "delete", &oldData, nil)

	c.Status(fiber.StatusNoContent)
	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Role deleted",
		Data:    record,
	})
}
