package controllers

import (
	"github.com/gofiber/fiber/v2"
	"math"
	"sidat-auth/database"
	"sidat-auth/models"
	"sidat-auth/models/reqresp"
	"sidat-auth/util"
	"strconv"
)

// AllUsers User list
// @Summary User list
// @Description User list
// @Tags Users
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.DxDatagridResponse
// @Router /users [get]
func AllUsers(c *fiber.Ctx) error {
	limit, _ := strconv.Atoi(c.Query("take", "10"))
	offset, _ := strconv.Atoi(c.Query("skip", "0"))
	page := math.Ceil(float64(offset/limit)) + 1
	var total int64

	var users []models.User

	database.DB.Preload("Role").Preload("Direktorat").Offset(offset).Limit(limit).Find(&users)

	database.DB.Model(&models.User{}).Count(&total)

	lastPage := math.Ceil(float64(int(total) / limit))
	if lastPage <= 0 {
		lastPage = 1
	}

	return c.JSON(&reqresp.DxDatagridResponse{
		Data:       users,
		TotalCount: int(total),
		Summary:    nil,
		Meta: fiber.Map{
			"total":     total,
			"page":      page,
			"last_page": lastPage,
		},
	})
}

// CreateUser Create a user
// @Summary Create a user
// @Description Create a user
// @Tags Users
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param user body models.User true "User data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /users [post]
func CreateUser(c *fiber.Ctx) error {
	var record models.User

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	record.SetPassword(record.Email)

	result := database.DB.Create(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to create record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "user", "insert", nil, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success creating record",
		Data:    record,
	})
}

// GetUser Get single user data
// @Summary Get single user data
// @Description Get single user data
// @Tags Users
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "User ID"
// @Success 200 {object} models.User
// @Error 404 {object} reqresp.ErrorResponse
// @Router /users/{id} [get]
func GetUser(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	user := models.User{Id: uint(id)}

	result := database.DB.Preload("Role").Preload("Direktorat").Find(&user)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(user)
}

// GetUserByEmail Get User Data by Email
// @Summary Get User Data by Email
// @Description Get User Data by Email
// @Tags Users
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param email query string true "User email"
// @Success 200 {object} models.User
// @Error 404 {object} reqresp.ErrorResponse
// @Router /users/by-email [get]
func GetUserByEmail(c *fiber.Ctx) error {
	email := c.Query("email")

	user := models.User{Email: email}

	result := database.DB.Preload("Role").Preload("Direktorat").Find(&user)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(user)
}

// UpdateUser Update a user
// @Summary Update a user
// @Description Update a user
// @Tags Users
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "User ID"
// @param user body models.User true "User data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 404 {object} reqresp.ErrorResponse
// @Router /users/{id} [put]
func UpdateUser(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.User{Id: uint(id)}

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	var oldData models.User
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Preload("Role").Preload("Direktorat").Model(&record).Updates(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	// send to log
	util.SendToAudit(c, "user", "update", &oldData, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success updating record",
		Data:    record,
	})
}

// DeleteUser Delete a user
// @Summary Delete a user
// @Description Delete a user
// @Tags Users
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "User ID"
// @Success 204 {object} reqresp.SuccessResponse
// @Error 500 {object} reqresp.ErrorResponse
// @Router /users/{id} [delete]
func DeleteUser(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.User{Id: uint(id)}

	var oldData models.User
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Delete(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to delete record",
		})
	}

	// send to log
	util.SendToAudit(c, "user", "delete", &oldData, nil)

	c.Status(fiber.StatusNoContent)
	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "record deleted",
		Data:    record,
	})
}
