package controllers

import (
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"gorm.io/gorm/clause"
	"os"
	"sidat-auth/database"
	"sidat-auth/models"
	"sidat-auth/models/reqresp"
	"strconv"
	"strings"
	"time"
)

type RegisterData struct {
	FirstName       string `json:"first_name"`
	LastName        string `json:"last_name"`
	Email           string `json:"email"`
	Password        string `json:"password"`
	PasswordConfirm string `json:"password_confirm"`
}

// Register Register new user
// @Summary Register new user
// @Description Register new user
// @Tags Auth
// @Accept json
// @Produce json
// @Param registerData body RegisterData true "Registration data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /auth/register [post]
func Register(c *fiber.Ctx) error {
	var data RegisterData
	err := c.BodyParser(&data)
	if err != nil {
		return err
	}

	if data.Password != data.PasswordConfirm {
		c.Status(400)
		return c.JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Password does not match",
		})
	}

	user := models.User{
		FirstName:    data.FirstName,
		LastName:     data.LastName,
		Email:        data.Email,
		RoleId:       1,
		DirektoratId: 1,
	}
	user.SetPassword(data.Password)

	database.DB.Create(&user)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Registrasi sukses",
		Data:    user,
	})
}

type userLogin struct {
	// you can use email, NIK or NIP to login
	Email    string
	Password string
}

// Login User login
// @Summary User login
// @Description User login
// @Tags Auth
// @Accept json
// @Produce json
// @Param loginData body userLogin true "User login data"
// @Success 200 {object} reqresp.TokenResponse
// @Error 400 {0bject} reqresp.ErrorResponse
// @Error 404 {0bject} reqresp.ErrorResponse
// @Router /auth/login [post]
func Login(c *fiber.Ctx) error {
	//viper.SetConfigFile(".env")
	//err := viper.ReadInConfig()
	//if err != nil {
	//	panic(fmt.Errorf("Fatal error config file: %w \n", err))
	//}

	var data userLogin
	if err := c.BodyParser(&data); err != nil {
		return err
	}

	var user models.User

	//check login by email
	database.DB.Preload(clause.Associations).Where("Email=? AND is_active=?", data.Email, true).First(&user)

	if user.Id == 0 {
		// check NIK
		database.DB.Preload(clause.Associations).Where("nik=? AND is_active=?", data.Email, true).First(&user)
		if user.Id == 0 {
			// check NIP
			database.DB.Preload(clause.Associations).Where("nip=? AND is_active=?", data.Email, true).First(&user)
			if user.Id == 0 {
				c.Status(404)
				return c.JSON(&reqresp.ErrorResponse{
					Message: "User not found",
					Status:  "error",
				})
			}

		}

	}

	err := user.ComparePassword(data.Password)
	if err != nil {
		c.Status(400)
		return c.JSON(&reqresp.ErrorResponse{
			Message: "Incorrect password",
			Status:  "error",
		})
	}

	var theRole models.Role
	database.DB.Preload(clause.Associations).Where("id = ?", user.RoleId).First(&theRole)

	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	tokenAgeHour, _ := strconv.Atoi(os.Getenv("TOKEN_AGE_HOUR"))

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["email"] = user.Email
	claims["user_id"] = user.Id
	claims["role"] = theRole
	claims["direktorat"] = user.Direktorat
	claims["kind"] = "access"
	claims["exp"] = time.Now().Add(time.Hour * time.Duration(tokenAgeHour)).Unix()

	// Generate encoded token and send it as reqresp.
	t, err := token.SignedString([]byte(os.Getenv("TOKEN_SECRET")))
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	// Create refresh token
	refreshToken := jwt.New(jwt.SigningMethodHS256)

	refreshTokenAgeHour, _ := strconv.Atoi(os.Getenv("REFRESH_TOKEN_AGE_HOUR"))

	// Set claims
	refreshClaims := refreshToken.Claims.(jwt.MapClaims)
	refreshClaims["email"] = user.Email
	refreshClaims["user_id"] = user.Id
	refreshClaims["role"] = theRole
	refreshClaims["direktorat"] = user.Direktorat
	refreshClaims["kind"] = "refresh"
	refreshClaims["exp"] = time.Now().Add(time.Hour * time.Duration(refreshTokenAgeHour)).Unix()

	// Generate encoded refresh token and send it as reqresp.
	rT, err := refreshToken.SignedString([]byte(os.Getenv("TOKEN_SECRET")))
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	cookie := fiber.Cookie{
		Name:  "jwt",
		Value: t,
		//Path:     "",
		//Domain:   "",
		//MaxAge:   0,
		Expires: time.Now().Add(time.Hour * time.Duration(tokenAgeHour)),
		//Secure:   false,
		HTTPOnly: true,
		//SameSite: "",
	}

	c.Cookie(&cookie)

	return c.JSON(&reqresp.TokenResponse{
		Message:      "success",
		AccessToken:  t,
		RefreshToken: rT,
	})

	//token, err := util.GenerateJwt(strconv.Itoa(int(user.Id)))
	//if err != nil {
	//	//return c.SendStatus(fiber.StatusInternalServerError)
	//	c.Status(fiber.StatusInternalServerError)
	//	return c.JSON(fiber.Map{
	//		"message": "failed to sign claims",
	//	})
	//}

	//cookie := fiber.Cookie{
	//	Name:     "jwt",
	//	Value:    token,
	//	//Path:     "",
	//	//Domain:   "",
	//	//MaxAge:   0,
	//	Expires:  time.Now().Add(time.Hour * 24),
	//	//Secure:   false,
	//	HTTPOnly: true,
	//	//SameSite: "",
	//}
	//
	//c.Cookie(&cookie)
	//
	//return c.JSON(fiber.Map{
	//	"message": "success",
	//})
}

type Claims struct {
	jwt.StandardClaims
}

// User Get logged in User info
// @Summary Get logged in User info
// @Description Get logged in User info
// @Tags Auth
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @Success 200 {object} models.User
// @Router /auth/user [get]
func User(c *fiber.Ctx) error {
	jwtUser := c.Locals("user").(*jwt.Token)
	jwtClaims := jwtUser.Claims.(jwt.MapClaims)

	//cookie := c.Cookies("jwt")
	//
	//id, _ := util.ParseJwt(cookie)

	var user models.User
	database.DB.Preload("Role").Preload("Direktorat").Where("id = ?", jwtClaims["user_id"]).First(&user)

	return c.JSON(user)
}

func Logout(c *fiber.Ctx) error {
	cookie := fiber.Cookie{
		Name:  "jwt",
		Value: "",
		//Path:     "",
		//Domain:   "",
		//MaxAge:   0,
		Expires: time.Now().Add(-time.Hour),
		//Secure:   false,
		HTTPOnly: true,
		//SameSite: "",
	}

	c.Cookie(&cookie)

	return c.JSON(fiber.Map{
		"message": "logout success",
	})
}

// UpdateInfo Update User Profile
// @Summary Update User Profile
// @Description Update User Profile
// @Tags Users
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @Param user body models.User true "User data"
// @Success 200 {object} models.User
// @Router /users/update-profile [post]
func UpdateInfo(c *fiber.Ctx) error {
	var data map[string]string
	if err := c.BodyParser(&data); err != nil {
		return err
	}

	//cookie := c.Cookies("jwt")
	//
	//id, _ := util.ParseJwt(cookie)
	//
	//userId, _ := strconv.Atoi(id)

	jwtUser := c.Locals("user").(*jwt.Token)
	jwtClaims := jwtUser.Claims.(jwt.MapClaims)

	userId := int(jwtClaims["user_id"].(float64))

	user := models.User{
		Id:        uint(userId),
		FirstName: data["first_name"],
		LastName:  data["last_name"],
		Email:     data["email"],
		Nip:       data["nip"],
		NoTelp:    data["no_telp"],
	}

	database.DB.Model(&user).Updates(user)
	database.DB.Preload("Role").Preload("Direktorat").Where("id", uint(userId)).First(&user)

	return c.JSON(user)
}

type updatePassword struct {
	Password        string
	PasswordConfirm string
}

// UpdatePassword Update User Password
// @Summary Update User Password
// @Description Update User Password
// @Tags Auth
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @Param newPassword body updatePassword true "New Password data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {0bject} reqresp.ErrorResponse
// @Router /auth/password [post]
func UpdatePassword(c *fiber.Ctx) error {
	var data updatePassword
	if err := c.BodyParser(&data); err != nil {
		return err
	}

	if data.Password != data.PasswordConfirm {
		c.Status(400)
		return c.JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Password does not match",
		})
	}
	//cookie := c.Cookies("jwt")
	//
	//id, _ := util.ParseJwt(cookie)
	//
	//userId, _ := strconv.Atoi(id)

	jwtUser := c.Locals("user").(*jwt.Token)
	jwtClaims := jwtUser.Claims.(jwt.MapClaims)

	userId := int(jwtClaims["user_id"].(float64))

	user := models.User{
		Id: uint(userId),
	}
	user.SetPassword(data.Password)
	database.DB.Model(&user).Updates(user)
	database.DB.Preload("Role").Preload("Direktorat").Where("id", uint(userId)).First(&user)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success updating password",
		Data:    user,
	})
}

// RefreshToken Generate new access token
// @Summary Generate new access token
// @Description Generate new access token
// @Tags Token
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @Success 200 {object} reqresp.TokenResponse
// @Error 400 {0bject} reqresp.ErrorResponse
// @Router /token/refresh [post]
func RefreshToken(c *fiber.Ctx) error {
	jwtString := strings.Split(c.Get("Authorization"), " ")[1]
	//jwtUser := c.Locals("user").(*jwt.Token)
	//viper.SetConfigFile(".env")
	//err := viper.ReadInConfig()
	//if err != nil {
	//	panic(fmt.Errorf("Fatal error config file: %w \n", err))
	//}

	var jwtClaims jwt.MapClaims
	_, err := jwt.ParseWithClaims(jwtString, &jwtClaims, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("TOKEN_SECRET")), nil
	})
	//fmt.Println(jwtUser)
	//jwtUser := jwtString.(*jwt.Token)
	//jwtClaims := jwtUser.Claims.(jwt.MapClaims)

	tokenKind := jwtClaims["kind"]

	if tokenKind != "refresh" {
		c.Status(400)
		return c.JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Wrong token type",
		})
	}

	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	tokenAgeHour, _ := strconv.Atoi(os.Getenv("TOKEN_AGE_HOUR"))

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["email"] = jwtClaims["email"]
	claims["user_id"] = jwtClaims["user_id"]
	claims["role"] = jwtClaims["role"]
	claims["direktorat"] = jwtClaims["direktorat"]
	claims["kind"] = "access"
	claims["exp"] = time.Now().Add(time.Hour * time.Duration(tokenAgeHour)).Unix()

	// Generate encoded token and send it as reqresp.
	t, err := token.SignedString([]byte(os.Getenv("TOKEN_SECRET")))
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	// Create refresh token
	refreshToken := jwt.New(jwt.SigningMethodHS256)

	refreshTokenAgeHour, _ := strconv.Atoi(os.Getenv("REFRESH_TOKEN_AGE_HOUR"))

	// Set claims
	refreshClaims := refreshToken.Claims.(jwt.MapClaims)
	refreshClaims["email"] = jwtClaims["email"]
	refreshClaims["user_id"] = jwtClaims["user_id"]
	refreshClaims["role"] = jwtClaims["role"]
	refreshClaims["direktorat"] = jwtClaims["direktorat"]
	refreshClaims["kind"] = "refresh"
	refreshClaims["exp"] = time.Now().Add(time.Hour * time.Duration(refreshTokenAgeHour)).Unix()

	// Generate encoded refresh token and send it as reqresp.
	rT, err := refreshToken.SignedString([]byte(os.Getenv("TOKEN_SECRET")))
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.JSON(&reqresp.TokenResponse{
		Message:      "success",
		AccessToken:  t,
		RefreshToken: rT,
	})
}

// InspectToken Inspect JWT Token
// @Summary Inspect JWT Token
// @Description Inspect JWT Token
// @Tags Token
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @Success 200 {object} jwt.Token
// @Router /token/inspect [post]
func InspectToken(c *fiber.Ctx) error {
	jwtUser := c.Locals("user").(*jwt.Token)

	return c.JSON(jwtUser)
}
