package routes

import (
	"github.com/gofiber/fiber/v2"
	jwtware "github.com/gofiber/jwt/v3"
	"os"
	"sidat-auth/controllers"
)

func Setup(app *fiber.App) {
	app.Post("/api/auth/register", controllers.Register)
	app.Post("/api/auth/login", controllers.Login)

	app.Post("/api/token/refresh", controllers.RefreshToken)

	// must be logged-in
	//app.Use(middleware.IsAuthenticated)
	app.Use(jwtware.New(jwtware.Config{
		SigningKey: []byte(os.Getenv("TOKEN_SECRET")),
	}))
	app.Get("/api/auth/user", controllers.User)
	app.Post("/api/auth/logout", controllers.Logout)
	app.Post("/api/auth/password", controllers.UpdatePassword)

	app.Post("/api/token/inspect", controllers.InspectToken)

	app.Get("/api/users", controllers.AllUsers)
	app.Post("/api/users", controllers.CreateUser)
	app.Post("/api/users/update-profile", controllers.UpdateInfo)
	app.Get("/api/users/by-email", controllers.GetUserByEmail)
	app.Get("/api/users/:id", controllers.GetUser)
	app.Put("/api/users/:id", controllers.UpdateUser)
	app.Delete("/api/users/:id", controllers.DeleteUser)

	app.Get("/api/roles", controllers.AllRoles)
	app.Post("/api/roles", controllers.CreateRole)
	app.Get("/api/roles/:id", controllers.GetRole)
	app.Put("/api/roles/:id", controllers.UpdateRole)
	app.Delete("/api/roles/:id", controllers.DeleteRole)

	app.Get("/api/permissions", controllers.AllPermissions)
	app.Post("/api/permissions", controllers.CreatePermission)
	app.Get("/api/permissions/check", controllers.CheckPermission)
	app.Get("/api/permissions/:id", controllers.GetPermission)
	app.Put("/api/permissions/:id", controllers.UpdatePermission)
	app.Delete("/api/permissions/:id", controllers.DeletePermission)

	app.Get("/api/notifikasi", controllers.AllNotifikasi)
	app.Post("/api/notifikasi", controllers.CreateNotifikasi)
	app.Get("/api/notifikasi/:id", controllers.GetNotifikasi)
	app.Put("/api/notifikasi/:id", controllers.UpdateNotifikasi)
	app.Delete("/api/notifikasi/:id", controllers.DeleteNotifikasi)
}
