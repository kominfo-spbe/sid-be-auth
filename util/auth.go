package util

import (
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"sidat-auth/models"
)

func IsAllowed(c *fiber.Ctx, permName string) (bool, error) {
	jwtUser := c.Locals("user").(*jwt.Token)
	jwtClaims := jwtUser.Claims.(jwt.MapClaims)

	marshal, _ := json.Marshal(jwtClaims["role"])
	role := models.Role{}
	err := json.Unmarshal(marshal, &role)
	if err != nil {
		return false, err
	}

	for _, permission := range role.Permission {
		if permission.Name == permName {
			return true, nil
		}
	}

	return false, nil
}
