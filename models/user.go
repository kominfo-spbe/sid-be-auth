package models

import (
	"golang.org/x/crypto/bcrypt"
	"time"
)

type User struct {
	Id           uint       `json:"id" gorm:"primaryKey"`
	CreatedAt    time.Time  `json:"created_at"`
	UpdatedAt    time.Time  `json:"updated_at"`
	FirstName    string     `json:"first_name"`
	LastName     string     `json:"last_name"`
	Email        string     `json:"email" gorm:"unique"`
	Nip          string     `json:"nip" gorm:"unique"`
	Nik          string     `json:"nik" gorm:"unique"`
	NoTelp       string     `json:"no_telp" gorm:"unique"`
	Password     []byte     `json:"-"`
	IsActive     bool       `json:"is_active" gorm:"default:false"`
	RoleId       uint       `json:"role_id"`
	Role         Role       `json:"role" gorm:"foreignKey:RoleId;constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	DirektoratId uint       `json:"direktorat_id"`
	Direktorat   Direktorat `json:"direktorat" gorm:"foreignKey:DirektoratId;constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
}

func (u *User) SetPassword(password string) {
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(password), 14)
	u.Password = hashedPassword
}

func (u *User) ComparePassword(password string) error {
	return bcrypt.CompareHashAndPassword(u.Password, []byte(password))
}

func (u User) TableName() string {
	return "user_mgt.user"
}
