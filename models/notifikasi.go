package models

import "time"

type Notifikasi struct {
	Id        uint            `json:"id" gorm:"primaryKey"`
	CreatedAt time.Time       `json:"created_at"`
	UpdatedAt time.Time       `json:"updated_at"`
	UserId 	uint 	`json:"user_id"`
	User	User	`json:"user" gorm:"foreignKey:UserId;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
	Message string `json:"message"`
	Url string `json:"url"`
	IsRead bool `json:"is_read" gorm:"default:false"`
}

func (n Notifikasi) TableName() string {
	return "user_mgt.notifikasi"
}
